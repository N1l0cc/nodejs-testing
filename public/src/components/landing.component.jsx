import React from 'react';

import style from '../styles/landing.style.js';

class LandingComponent extends React.Component {

    render( ) {

        return (

            <div style={style}>

                This is the landing page, displayed on default when no other routes match.

            </div>

        );

    }

}

module.exports = LandingComponent ;
