import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import api from './api.json';

import LandingComponent from './components/landing.component.jsx';


class App extends React.Component {

    render( ) {
        return (

                <div>

                    <Router>

                        <Switch>

                            <Route component={LandingComponent}/>

                        </Switch>

                    </Router>

                </div>

        );

    }

}

ReactDOM.render(<App/>, document.getElementById( 'app' ));
