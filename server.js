var Express = require('express'),
    Path = require('path');

var port = process.env.PORT || 4040;

var express = Express();

// Serve static assets from the /public/static folder on default
express.use(Express.static(Path.join(__dirname, '/public/static')));


var httpServer = require('http').createServer(express);

httpServer.listen(port, () => {
    console.log('express running on port ' + port + '.');
});


